Product Pipeline Manager
==================================================


Features Summary
----------------

- The Pipeline Manager is accessible and visible from the Salesforce Opportunity which makes it easy for sales users to access.  
- The Pipeline Manager roles up custom pipeline line items total to the opportunity so users can see totals of pipeline revenue represented on the opportunity. 
- Automatically updates the opportunity amount up to the I/O stage which provides users with pipeline stages information. 
- Automatically updates the opportunity amount from the insertion orders after the I/O stage. 
- Sellers can select a product family, product, and revenue month from within the pipeline line item making it easy and fast for sellers. 
- Pipeline product and product families mirror Salesforce products which means that they can be fully customized. 


Usage Information and Known Issues
----------------------------------

- **Platform SOQL Limits** This tool uses DML and SOQL queries, which makes it subject to platform limitations
- **Deployment** This tool dynamically deploys a test factory, test classes, apex classes, apex triggers, custom objects, custom fields, validation rules, a process builder, a custom notification, and a static resource
- **Sandbox Testing** While the tool can be installed and enabled directly in production, sandbox testing is still strongly recommended.
- **Requirements** This tool requires account team and opportunity team for their deployment.

Documentation
----------------------------------
More details about the tool can be found [here](https://v2analyst.atlassian.net/wiki/spaces/ARCHDEV/pages/1387331661/Pipeline+Product+Manager)


Packaged Release History
------------------------

**Latest Release Version 0.1**
_______________________________

[Package URL]()

Installing the Source Code (Developers)
=======================================

This project uses Salesforce DX for development and packaging. If you want to deploy the unmanaged version of this to your sandbox or production org you can use the Salesforce DX convert and deploy commands to do so. However the recommended deployment for these orgs is via the managed package links above.
