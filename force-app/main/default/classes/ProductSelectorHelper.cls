public class ProductSelectorHelper {

    public static String NAMESPACEPREFIX {
        get {
            return getNamespace();
        }
    }

    /**
     * @description Get query string based off of a fields in field set.
     * @param searchCriteria This is the search criteria recieved as a JSON i.e. 
     * {
     *      searchCriteria : [ 
     *                          { Name :{type : 'text', value: 'text'} },
     *                          {Amount__c : {type: 'currency', value: '10.0'}},
     *                          {Id: {type: "id",value: "opportunitiId"}}
     *
     *                       ]
     * }
     * @param objectApiName The API name of the object to search
     * @param fieldSetApiName The name of the field set containing the fields for the search
     * @return String String containing the search query.
     */
    //Get query based on a fieldset
    public static String getQueryString(String searchCriteria,String objectApiName,String fieldSetApiName,Integer limitValue){
        //Getting the namespace
        String namespacePrefix = getNamespace();
        String queryString = 'SELECT ID,';
        String dynamicWhereCaluse = ' WHERE ';
        try {
            if(String.isNotBlank(searchCriteria)){
                    //Deserializing JSON searchCriteria
                Map<String,Object> criteriaMap = (Map<String,Object>)JSON.deserializeUntyped(searchCriteria);
                if(criteriaMap.containsKey('searchCriteria')){
                    List<Object> searchCriteriaList = (List<Object>)criteriaMap.get('searchCriteria');
                    for(Integer i=0,size = searchCriteriaList.size(); i < size; i++){
                        Map<String,Object> fieldsObject = (Map<String,Object>)searchCriteriaList[i];
                        //Getting the field properties for the 
                        for(String fieldApikey: fieldsObject.keySet()){
                            Map<String, Object> fieldProperties = (Map<String,Object>)fieldsObject.get(fieldApikey);
                            if(fieldProperties.containsKey('type') && String.valueOf(fieldProperties.get('type')).containsIgnoreCase('text')){
                                dynamicWhereCaluse += fieldApikey + ' LIKE \'%' + fieldProperties.get('value') + '%\' ' + 'AND '; 
                            } else if(fieldProperties.containsKey('type') && String.valueOf(fieldProperties.get('type')).containsIgnoreCase('Id')){
                                dynamicWhereCaluse += fieldApikey + '=\'' + fieldProperties.get('value') + '\'' + 'AND ';
                            } else {
                                dynamicWhereCaluse += fieldApikey + '=' + fieldProperties.get('value') + ' ' + 'AND ';
                            }
                        }
                    }
                }
                
                //Removing the last AND of the concatenation
                dynamicWhereCaluse = dynamicWhereCaluse.removeEnd('AND ');
            }
            
            //TODO: Adding the fields through a Helper Class for getting fields of the tables with the field set
            String namespacePrefixofieldSetName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + fieldSetApiName) : fieldSetApiName;
            String namespacePrefixobjectName =  (String.isNotBlank(namespacePrefix))? (namespacePrefix + objectApiName) : objectApiName;
            if(String.isNotBlank(fieldSetApiName)){
                List<FieldSetWrapper> fieldSetFieldsList = 
                getObjectFieldSetFields((namespacePrefixofieldSetName),(namespacePrefixobjectName));

                //Passing the field to the query
                String fieldsToQuery = '';
                for(FieldSetWrapper fieldSetWrapper: fieldSetFieldsList){
                    fieldsToQuery += fieldSetWrapper.fieldApiName + ',';
                }                                                                
                //Removing last comma
                fieldsToQuery = fieldsToQuery.removeEnd(',');

                //Adding the fields
                queryString += fieldsToQuery;
            } else {
                queryString = queryString.remove(',');
            }

            //Adding the Object
            queryString += ' FROM ' + namespacePrefixobjectName;

            //Adding the where clause
            queryString += dynamicWhereCaluse;

            if(limitValue != null && limitValue > 0){
                queryString += ' LIMIT ' + String.valueOf(limitValue);
            }
        }  catch(Exception e){
            System.debug('productSelectorHelper -> ' + e.getMessage());
            throw new ProductSelectorException(e.getMessage());
        }

        return queryString;
    }

    /***
     * @description This method will get the field set for getting the columns for the table.
     * @param fieldSetName This is the API name of the field set.
     * @param objectApiName This is the name of the object that will use the field set.
     * @return This method will return a list of FieldSetWrapper for manipulation of the field in the table.
     */
    public static List<FieldSetWrapper> getObjectFieldSetFields(String fieldSetName, String objectApiName){
        List<FieldSetWrapper> fieldsWrapperList = new List<FieldSetWrapper>();
        try {
            Map<String, Schema.SObjectType> objectApiNameBySobjectTypeMap = Schema.getGlobalDescribe();
            SObjectType objectType = objectApiNameBySobjectTypeMap.get(objectApiName);
            DescribeSobjectResult resultObject = objectType.getDescribe();
            Map<String,Schema.FieldSet> objectFieldSetsMap = resultObject.fieldSets.getMap();
            FieldSet objectFieldSet = objectFieldSetsMap.get(fieldSetName);
            for(FieldSetMember fieldMember: objectFieldSet.getFields()){
                fieldsWrapperList.add(new FieldSetWrapper(fieldMember.getLabel(),
                                                        getColumnType(fieldMember.getType()),
                                                        fieldMember.getFieldPath()
                                                        ));
            }

        } catch (Exception e){
            throw new ProductSelectorException('Error: ' + e.getMessage(),e);
        }
        return fieldsWrapperList;
    }

    /**
     * @description This method gets the namespace from the Product Selector Helper class
     * @return This methos reutrns the namespace with the underscores.
     */
    public static String getNamespace(){
        String namespace = '';
        namespace = [Select Id, Namespaceprefix FROM ApexClass WHERE Name = 'ProductSelectorHelper'].Namespaceprefix;
        if(String.isNotBlank(namespace)){
            namespace = namespace + '__';
        }
        return namespace;

    }
    
    /**
     * @description This method is used to get the type of column that will be set on the search
     * @param columnType This is the a param of type DisplayType ENUM
     * @return This method will return a string which is the column type
     */
    private static String getColumnType(DisplayType columnType){
        switch on columnType {
            when STRING {
                return 'text';
            }

            when else {
                return columnType.name().toLowerCase();
            }

        }
    }

    public static Map<Id,Product_Selector__mdt> getProductSelectorMetadataTypeRecords(){
        Schema.DescribeSObjectResult productSelectorResult = Product_Selector__mdt.SObjectType.getDescribe();
        Map<String,Schema.SObjectField> productSelectorMetadataFieldsMap = productSelectorResult.fields.getMap();
        String queryString = 'SELECT ';
        String fieldsSeperatedByComma = '';
        for(String fieldApiName: productSelectorMetadataFieldsMap.keySet()){
            fieldsSeperatedByComma += fieldApiName + ',';
        }
        fieldsSeperatedByComma = fieldsSeperatedByComma.removeEnd(',');
        queryString += fieldsSeperatedByComma;
        queryString += ' FROM Product_Selector__mdt';
        List<Product_Selector__mdt> productSelectorList = Database.query(queryString);
        Map<Id,Product_Selector__mdt> proudctSelectorMap = new Map<Id,Product_Selector__mdt>(productSelectorList);
        return proudctSelectorMap;
    }

    public static Product_Selector__mdt getMetadataRecord(String developerName){
        Map<Id,Product_Selector__mdt> productSelectorMap = getProductSelectorMetadataTypeRecords();
        Product_Selector__mdt selectedType = null;
        for(Id customMetadataId: productSelectorMap.keySet()){
            Product_Selector__mdt selectorMetadataType = productSelectorMap.get(customMetadataId);
            if(selectorMetadataType.DeveloperName == developerName){
                selectedType = selectorMetadataType;
                break;
            }
        }
        return selectedType;
    }

    public static Map<String,String> getPicklistValuesFromObject(String objectApiName, String fieldApiName){
        Map<String,String> valueApiNameByLabelMap = new Map<String,String>();
        String namespacePrefix = NAMESPACEPREFIX;
        String objectNameWithNamespace = (String.isNotBlank(namespacePrefix))? (namespacePrefix + objectApiName):objectApiName;
        String fieldName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + fieldApiName): fieldApiName;
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectNameWithNamespace) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> picklistValueList = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistValueList){
            valueApiNameByLabelMap.put(picklistVal.getValue(),pickListVal.getLabel());
        }
        return valueApiNameByLabelMap;
    }

    public static Map<String,RecordTypeInfo> getRecordTypes(String objectApiName){
        String objectNameWithNamespace = (String.isNotBlank(NAMESPACEPREFIX))? (NAMESPACEPREFIX+ objectApiName):objectApiName;
        List<String> objectNames = new List<String>{objectNameWithNamespace};
        DescribeSobjectResult results = Schema.describeSObjects(objectNames)[0];
        return results.getRecordTypeInfosByDeveloperName();
    }

    /**Receiving the list: 
     * [
     *      {    Amount__c:10000, End_Date__c:2021-02-05, Family__c:Digital, 
     *           Id:a024x000003yHA8AAM, Start_Date__c:2021-02-01, Status__c:Pipeline
     *      }
     * ]
    */
    public static void saveLineItems(String jsonProductList,Id opportunityId){
        List<Pipeline_Product_Line_Item__c> pipelineLineItemsToInsert = new List<Pipeline_Product_Line_Item__c>();
       //Pase the JSON
        List<Object> productListParsed = (List<Object>)JSON.deserializeUntyped(jsonProductList);
        for(Object productObjectRecord: productListParsed){
            //Get the dates
            Map<String,Object> parsingtheProductObject = (Map<String,Object>)productObjectRecord;
            Date startDate = Date.valueOf(String.valueOf(parsingtheProductObject.get('Start_Date__c')));
            Date endDate = Date.valueOf(String.valueOf(parsingtheProductObject.get('End_Date__c')));
            //Plus one to include the last day
            Integer totalNumberOfDays = startDate.daysBetween(endDate)+1;
            System.debug('TOTAL NUMBER OF DAYS >> ' + totalNumberOfDays);
            //Calculating the months
            //Plus 1 To include the end date add 1
            Integer monthsBetween = (startDate.monthsBetween(endDate) == 0)? 1:startDate.monthsBetween(endDate)+1;
            //Getting the total amount
            Decimal productTotalAmount = Decimal.valueOf(String.valueOf(parsingtheProductObject.get('Amount__c')));
            //The Product Status
            String productStatus = String.valueOf(parsingtheProductObject.get('Status__c'));
            //The product Id
            String productId = String.valueOf(parsingtheProductObject.get('Id'));

            //For loop for generating the line items based on the months
            Integer monthCounter = startDate.month();
            Integer yearCounter = startDate.year();
            Integer dayCounter = 0;
            Decimal totalCount = 0.0;
            Decimal differenceCents = 0.0;
            for(Integer i =1; i <= monthsBetween; i++){

                Pipeline_Product_Line_Item__c pipelineLineItem = new Pipeline_Product_Line_Item__c();
                //This is for when its an event because its the same start Date and end date
                if(totalNumberOfDays == 1){
                    pipelineLineItem.Amount__c = productTotalAmount;
                    pipelineLineItem.Start_Date__c = startDate;
                    pipelineLineItem.End_Date__c = endDate;
                    pipelineLineItem.Pipeline_Product__c = productId;
                    pipelineLineItem.Opportunity__c = opportunityId;
                    pipelineLineItem.Status__c = productStatus;
                } else {
                    Integer dayToStart = ( i == 1)? startDate.day(): 1;
                    //Getting the current month 
                    Date currentStartDateMonth = Date.newInstance(yearCounter, monthCounter, dayToStart);
                    //Getting the total days of the current month
                    Integer totalDaysOfTheMonth = (Date.daysInMonth(currentStartDateMonth.year(),monthCounter));
                    //Getting the current month end date
                    Date currentMonthEndate = Date.newInstance(currentStartDateMonth.year(), monthCounter, totalDaysOfTheMonth);
                    //Getting the days between the current start day and the end date
                    Integer daysBetween = currentStartDateMonth.daysBetween(currentMonthEndate)+1;
                    //Calculation the percent that will be taken from the total amount on the product
                    Decimal percentOfTheTotal = (Decimal.valueOf(daysBetween)/Decimal.valueOf(totalNumberOfDays));
                    //Getting only the 4 digits after the decimal point
                    percentOfTheTotal = percentOfTheTotal.setScale(4);
                    //Gettting the full percentage for the distribution
                    Decimal percentFromFranctionToDecimal = percentOfTheTotal*100;
                    //The value rounded and then converted to fraction
                    Decimal valueRounded = percentFromFranctionToDecimal.round(System.RoundingMode.HALF_EVEN)/100.0;
                    //Caculating the total amount for the line
                    Decimal totalForLine = valueRounded * productTotalAmount;
                    //Rounding down the total to eliminate the fractional part (after the decimal point)
                    Decimal roundedDownTotal = totalForline.round(System.RoundingMode.DOWN);
                    //Acumalator of the total
                    totalCount += roundedDownTotal;
                    
                    //Creating the pipelineLineItem
                    pipelineLineItem.Amount__c = roundedDownTotal;
                    pipelineLineItem.Start_Date__c = currentStartDateMonth;
                    pipelineLineItem.End_Date__c = currentMonthEndate;
                    pipelineLineItem.Pipeline_Product__c = productId;
                    pipelineLineItem.Opportunity__c = opportunityId;
                    pipelineLineItem.Status__c = productStatus;

                    //Incrementing through the months
                    monthCounter++;
                    //When december
                    if(monthCounter == 13){
                        monthCounter = 1;
                        yearCounter++;
                    }

                    //If statement to check for any remainder and adding to the last month
                    if(i == monthsBetween ){
                        pipelineLineItem.End_Date__c = endDate;
                        Decimal differenceOfEndMonth = productTotalAmount - totalCount;
                        if(differenceOfEndMonth != 0 ){
                            pipelineLineItem.Amount__c = roundedDownTotal + differenceOfEndMonth;  
                        }
                    }
                }
                
                pipelineLineItemsToInsert.add(pipelineLineItem);
            }
        }
        
        if(!pipelineLineItemsToInsert.isEmpty()){
            insert pipelineLineItemsToInsert;
        }

    }


    public class FieldSetWrapper {

        @AuraEnabled
        public String label {get; set;}

        @AuraEnabled
        public String type {get; set;}

        @AuraEnabled
        public String fieldApiName {get; set;}

        @AuraEnabled
        public FieldSetMember member {get; set;}

        public FieldSetWrapper(String label, String type, String fieldApiName,FieldSetMember member){
            this.label = label;
            this.type = type;
            this.fieldApiName = fieldApiName;
            this.member = member;
        }

        public FieldSetWrapper(String label, String type, String fieldApiName){
            this.label = label;
            this.type = type;
            this.fieldApiName = fieldApiName;
        }
    }
    
    public class ProductSelectorException extends Exception  {
        
        public String getMessageException(String errorMsg){
        	throw new ProductSelectorException(errorMsg);
        }
    }
}