@isTest(SeeAllData=true)
public class ProductSelectorManagerControllerTest {
    /*@TestSetup
    static void setUp(){
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.StageName = 'Prospecting';
        o.Vertical__c = 'Auto';
        o.CloseDate = Date.newInstance(2021, 02, 15);
        insert o;
		System.debug(o);
        Pipeline_Product__c pp = new Pipeline_Product__c();
        pp.Name = 'Test Pipeline Product';
        insert pp;
		System.debug(pp);
        List<Pipeline_Product_Line_Item__c> ppliList = new List<Pipeline_Product_Line_Item__c>();
        for(Integer i = 0; i < 2; i++){
            Pipeline_Product_Line_Item__c ppli = new Pipeline_Product_Line_Item__c();
            ppli.Pipeline_Product__c = pp.Id;
            ppli.Opportunity__c = o.Id;
            ppli.Amount__c = i;
            ppli.Start_Date__c = System.today();
            ppliList.add(ppli);
        }
        insert ppliList;
        System.debug(ppliList);
    }*/

    @isTest
    private static void getPipeLineProductLineItemsPositiveTest(){
		Test.startTest();
        	Opportunity o = [SELECT Id FROM Opportunity limit 1];
        	Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
        	Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
        	pplid.Pipeline_Product__c = pp.Id;
            pplid.Opportunity__c = o.Id;
            pplid.Amount__c = 1000;
            pplid.Start_Date__c = System.today();
        	insert pplid;
        	Pipeline_Product_Line_Item__c ppli = [select Id, Opportunity__c from Pipeline_Product_Line_Item__c where Opportunity__c <> null limit 1];
        	String searchCriteria = '{"searchCriteria":[{"Opportunity__c":{"type":"id","value":"'+ppli.Opportunity__c+'"}}]}';
        	String fieldSetApiName = 'Pipeline_Line_Item_Manager_Fieldset';
        	String objectApiName = 'Pipeline_Product_Line_Item__c';
        	List<Pipeline_Product_Line_Item__c> ppliList = ProductSelectorManagerController.getPipeLineProductLineItems(
            												searchCriteria,
                											objectApiName,
                											fieldSetApiName,
                											100);
        Test.stopTest();
        System.assertEquals(false, ppliList.isEmpty());
        System.assertNotEquals(0, ppliList.size());
    }
    
    @isTest
    private static void getPipelineLineItemTableColumnsPositiveTest(){
        Test.startTest();
        	List<ProductSelectorHelper.FieldSetWrapper> fsw = ProductSelectorManagerController.getPipelineLineItemTableColumns('Pipeline_Line_Item_Manager_Fieldset', 'Pipeline_Product_Line_Item__c');
        Test.stopTest();
        
        System.assertEquals(false, fsw.isEmpty());
        System.assertEquals(6, fsw.size());
    }
    
    @isTest
    private static void getPipeLineProductLineItemsPositiveTestNull(){
        Test.startTest();
        	Opportunity o = [SELECT Id FROM Opportunity limit 1];
        	Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
        	Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
        	pplid.Pipeline_Product__c = pp.Id;
            pplid.Opportunity__c = o.Id;
            pplid.Amount__c = 1000;
            pplid.Start_Date__c = System.today();
        	insert pplid;
        	String searchCriteria = '{"searchCriteria":[{"Opportunity__c":{"type":"id","value":"'+o.Id+'"}}]}';
        	String fieldSetApiName = 'Pipeline_Line_Item_Manager_Fieldset';
        	String objectApiName = 'Pipeline_Product_Line_Item__c';
        	List<Pipeline_Product_Line_Item__c> ppliList = ProductSelectorManagerController.getPipeLineProductLineItems(
            												searchCriteria,
                											objectApiName,
                											null,
                											100);
        Test.stopTest();
    }
    
    @isTest
    private static void getPipeLineProductLineItemsPositiveExceptionHandlerTest(){
        try{
            Test.startTest();
        	Opportunity o = [SELECT Id FROM Opportunity limit 1];
        	Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
        	Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
        	pplid.Pipeline_Product__c = pp.Id;
            pplid.Opportunity__c = o.Id;
            pplid.Amount__c = 1000;
            pplid.Start_Date__c = System.today();
        	insert pplid;
        	String searchCriteria = '{"searchCriteria":[{"Opportunit__c":{"type":"id","value":"'+o.Id+'"}}]}';
        	String fieldSetApiName = 'Pipeline_Line_Item_Manager_Fieldset';
        	String objectApiName = 'Pipeline_Product_Line_Item__c';
        	List<Pipeline_Product_Line_Item__c> ppliList = ProductSelectorManagerController.getPipeLineProductLineItems(
            												searchCriteria,
                											objectApiName,
                											null,
                											100);
        Test.stopTest();
        }catch(Exception e){
            System.assertEquals(true,e.getTypeName().containsIgnoreCase('ProductSelectorException'));
        }
        
    }
    
    @isTest
    private static void processPipelineProductLIDatePositiveTest(){
        Opportunity o = [SELECT Id FROM Opportunity limit 1];
        Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
        Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
        pplid.Pipeline_Product__c = pp.Id;
        pplid.Opportunity__c = o.Id;
        pplid.Amount__c = 1000;
        pplid.Start_Date__c = System.today();
        insert pplid;
       	Id pipelineLineItemId = [SELECT Id from Pipeline_Product_Line_Item__c LIMIT 1].Id;
        Test.startTest();
        ProductSelectorManagerController.processPipelineProductLIDate(pipelineLineItemId,2,2021);
        Test.stopTest();
        Pipeline_Product_Line_Item__c testProductPipelineLineItem = [SELECT Id,Start_Date__c from Pipeline_Product_Line_Item__c where Id =: pipelineLineItemId];
        System.assertEquals(2,testProductPipelineLineItem.Start_Date__c.month());
    }
    
    @isTest
    private static void getStatusValuesPositiveTest(){
        Test.startTest();
        Map<String,String> optionsMap = ProductSelectorManagerController.getStatusValues();
        Test.stopTest();
        System.assertEquals(false,optionsMap.isEmpty());
    }
    
    @isTest
    private static void saveStatusPositiveTest(){
        Opportunity o = [SELECT Id FROM Opportunity limit 1];
        Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
        Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
        pplid.Pipeline_Product__c = pp.Id;
        pplid.Opportunity__c = o.Id;
        pplid.Amount__c = 1000;
        pplid.Start_Date__c = System.today();
        insert pplid;
        Id pipelineLineItemId = [SELECT Id from Pipeline_Product_Line_Item__c LIMIT 1].Id;
        Test.startTest();
        ProductSelectorManagerController.saveStatus('Lost',pipelineLineItemId);
        Test.stopTest();
        Pipeline_Product_Line_Item__c testProductPipelineLineItem = [SELECT Id,Status__c from Pipeline_Product_Line_Item__c where Id =: pipelineLineItemId];
        System.assertEquals('Lost',testProductPipelineLineItem.Status__c);
        
    }
    
    @isTest
    private static void saveStatusPositiveExceptionHandlerTest(){
        try{
            Opportunity o = [SELECT Id FROM Opportunity limit 1];
            Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c limit 1];
            Pipeline_Product_Line_Item__c pplid = new Pipeline_Product_Line_Item__c();
            pplid.Pipeline_Product__c = pp.Id;
            pplid.Opportunity__c = o.Id;
            pplid.Amount__c = 1000;
            pplid.Start_Date__c = System.today();
            insert pplid;
            Id pipelineLineItemId = [SELECT Id from Pipeline_Product_Line_Item__c LIMIT 1].Id;
            Test.startTest();
            ProductSelectorManagerController.saveStatus('Lost',null);
            Test.stopTest();
            Pipeline_Product_Line_Item__c testProductPipelineLineItem = [SELECT Id,Status__c from Pipeline_Product_Line_Item__c where Id =: pipelineLineItemId];
        }catch(Exception e){
            System.assertEquals(true,e.getTypeName().containsIgnoreCase('ProductSelectorException'));
        }
    }
    
    @isTest
    private static void getNamespacePositiveTest(){
        Test.startTest();
        String namespace = ProductSelectorManagerController.getNamespace();
        Test.stopTest();  
    }
    
}