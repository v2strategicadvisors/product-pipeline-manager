@isTest(SeeAllData=true)
public class ProductSearchControllerTest {

    private static final Integer BULK_SIZE = 250;
    
    /*@TestSetup
    static void createTestData(){
        
        
        List<Pipeline_Brand__c> brandsList = new List<Pipeline_Brand__c>();
        Pipeline_Brand__c brandRecord = new Pipeline_Brand__c();
        brandRecord.Brand_Name__c = 'Test Brand';
        brandsList.add(brandRecord);
        insert brandsList;
        
        List<Pipeline_Product__c> pipelineProductList = new List<Pipeline_Product__c>();
        for(Integer i = 0, size = BULK_SIZE; i < size; i++){
            Pipeline_Product__c pipelineProduct = new Pipeline_Product__c();
            pipelineProduct.Name = 'Test ' + i;
            pipelineProduct.Pipeline_Brand__c = brandRecord.Id;
            pipelineProduct.Date__c = Date.newInstance(System.today().year(), System.today().month(), System.today().day());
            pipelineProductList.add(pipelineProduct);
        }
        insert pipelineProductList;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test opportunity';
        opp.Amount = 100000;
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.newInstance(2021, 05, 15);
        insert opp;
    }*/

    @isTest
    private static void getPipelineProductsPositiveTest(){
        Test.startTest();
        Pipeline_Brand__c brandRecord = new Pipeline_Brand__c();
        brandRecord.Brand_Name__c = 'Test Brand';
        insert brandRecord;
        
        Pipeline_Product__c pipelineProduct = new Pipeline_Product__c();
        pipelineProduct.Name = 'Test ';
        pipelineProduct.Pipeline_Brand__c = brandRecord.Id;
        pipelineProduct.Date__c = Date.newInstance(System.today().year(), System.today().month(), System.today().day());
        insert pipelineProduct;
        String searchCriteria = '{"searchCriteria":[{"Name":{"type":"text","value":"test"}}]}';
        List<Pipeline_Product__c> pipelineRecords = ProductSearchController.getPipeLineProducts(searchCriteria,'Pipeline_Product__c','Product_Selector_Field_Set',2000);
        Test.stopTest();
        System.assertEquals(false,pipelineRecords.isEmpty());
    }
    
    @isTest
    private static void getPipelineProductsNegativeTest(){
        try {
            Test.startTest();
        	String searchCriteria = '{"searchCriteria":[{"Name":{"type":"text","value":"test"}}]}';
        	List<Pipeline_Product__c> pipelineRecords = ProductSearchController.getPipeLineProducts(searchCriteria,null,'Product_Selector_Field_Set',2000);
        	Test.stopTest();
        }catch (Exception e){
            System.debug('Printing the actual information' + e.getTypeName());
        	System.assertEquals(true,e.getTypeName().containsIgnoreCase('ProductSelectorException'));   
        }
    }
    
    @isTest
    private static void getPipelineProducts_withUserBrands_PositiveTest(){
        
        Pipeline_Brand__c brandRecord = new Pipeline_Brand__c();
        brandRecord.Brand_Name__c = 'Test Brand';
        insert brandRecord;
        
        User_Brand__c userBrand = new User_Brand__c();
        userBrand.User__c = UserInfo.getUserId();
        userBrand.Pipeline_Brand__c = brandRecord.Id;
        insert userBrand;
        
        Test.startTest();
        String searchCriteria = '{"searchCriteria":[{"Name":{"type":"text","value":"test"}}]}';
        List<Pipeline_Product__c> pipelineRecords = ProductSearchController.getPipeLineProducts(searchCriteria,'Pipeline_Product__c','Product_Selector_Field_Set',2000);
        Test.stopTest();
    }
    

    @isTest
    private static void getPipelineProductTableColumnsPositiveTest(){
        Test.startTest();
        List<ProductSelectorHelper.FieldSetWrapper> fieldSetWrapperList = ProductSearchController.getPipelineProductTableColumns('Product_Selector_Field_Set','Pipeline_Product__c');
        Test.stopTest();
        System.assertEquals(false,fieldSetWrapperList.isEmpty());
    }

    @isTest
    private static void getPipelineLineItemProductTableColumnsPositiveTest(){
        Test.startTest();
        List<ProductSelectorHelper.FieldSetWrapper> fieldSetWrapperList = ProductSearchController.getPipelineLineItemProductTableColumns('Pipeline_Line_Item_Fieldset','Pipeline_Product_Line_Item__c');
        Test.stopTest();
        System.assertEquals(false,fieldSetWrapperList.isEmpty());
    }

    @isTest
    private static void checkForPickListValuesPositiveTest_reocrdTypeSetToTrue(){
        Test.startTest();
        Map<String,String> recordTypeOrValuesMap = ProductSearchController.checkForPickListValues('Pipeline_Product__c',null,'Family__c',true,'Master');
        Test.stopTest();
        System.assertEquals(false,recordTypeOrValuesMap.isEmpty());
        System.assertEquals(1,recordTypeOrValuesMap.size());
    }

    @isTest
    private static void checkForPickListValuesPositiveTest_reocrdTypeSetToFalse(){
        Test.startTest();
        Map<String,String> recordTypeOrValuesMap = ProductSearchController.checkForPickListValues('Pipeline_Product__c',null,'Family__c',false,'Master');
        Test.stopTest();
        System.assertEquals(false,recordTypeOrValuesMap.isEmpty());
        System.assert(recordTypeOrValuesMap.size() > 1 );
    }
    
    @isTest
    private static void checkForPickListValuesPositiveTest_reocrdTypeSetToFalseExceptionHandler(){
        
        try{
            Test.startTest();
            Map<String,String> recordTypeOrValuesMap = ProductSearchController.checkForPickListValues('Pipeline_Produc__c',null,'Family__c',false,'Master');
            Test.stopTest();
        }catch(Exception e){
        	System.assertEquals(true,e.getTypeName().containsIgnoreCase('ProductSelectorException')); 
        }
    }

    @isTest
    private static void getBrandOptions_UseBrandsSetToFalse_PositiveTest(){
        Product_Selector__mdt productSelector = [Select Id from Product_Selector__mdt where DeveloperName = 'Selector_Configuration'];
        Test.startTest();
        List<String> brandOptions = ProductSearchController.getBrandOptions(UserInfo.getUserId(),false);
        Test.stopTest();
        System.assertEquals(false,brandOptions.isEmpty());
       
    }

    @isTest
    private static void getBrandOptions_UseBrandsSetToTrue_PositiveTest(){
        Product_Selector__mdt productSelector = [Select Id,Use_User_Brands_Object__c from Product_Selector__mdt where DeveloperName = 'Selector_Configuration'];
        productSelector.Use_User_Brands_Object__c = true;

        Pipeline_Brand__c pipelineBrand = new Pipeline_Brand__c();
        pipelineBrand.Brand_Name__c = 'Test';
        insert pipelineBrand;

        User_Brand__c userBrandRecord = new User_Brand__c();
        userBrandRecord.Pipeline_Brand__c = pipelineBrand.Id;
        userBrandRecord.User__c = UserInfo.getUserId();
        insert userBrandRecord;

        Test.startTest();
        List<String> brandOptions = ProductSearchController.getBrandOptions(UserInfo.getUserId(),true);
        Test.stopTest();
        System.assertEquals(false,brandOptions.isEmpty());
       
    }
    
    @isTest
    private static void getBrandOptions_UseBrandsSetToTrue_noUserBrands_PositiveTest(){
        Product_Selector__mdt productSelector = [Select Id,Use_User_Brands_Object__c from Product_Selector__mdt where DeveloperName = 'Selector_Configuration'];
        productSelector.Use_User_Brands_Object__c = true;
        
        Test.startTest();
        List<String> brandOptions = ProductSearchController.getBrandOptions(UserInfo.getUserId(),true);
        Test.stopTest();
        System.assertEquals(false,brandOptions.isEmpty());
       
    }
    
    @isTest
    private static void getBrandOptionsExceptionHandler(){
        //Product_Selector__mdt productSelector = [Select Id,Use_User_Brands_Object__c from Product_Selector__mdt where DeveloperName = 'Selector_Configuration'];
        //productSelector.Use_User_Brands_Object__c = true;
        
        try{
            Test.startTest();
        	List<String> brandOptions = ProductSearchController.getBrandOptions(null,null);
        	Test.stopTest();
        }catch(Exception e){
        	System.assertEquals(true,e.getTypeName().containsIgnoreCase('ProductSelectorException'));
        }
    }


    @isTest
    private static void loadProductSelectorConfigurationPositiveTest(){
        Test.startTest();
        Product_Selector__mdt productSelectorConfiguration = ProductSearchController.loadProductSelectorConfiguration();
        Test.stopTest();
        System.assertEquals(true,String.isNotBlank(productSelectorConfiguration.Id));
       
    }

    @isTest
    private static void getNamespacePositiveTest(){
        String namespacePrefix;
        Test.startTest();
        namespacePrefix = ProductSearchController.getNamespace(); 
        if(namespacePrefix == null){
            namespacePrefix = 'V2SACL1';
        }
        Test.stopTest();
        System.assertEquals(false,String.isBlank(namespacePrefix));
    }
    
    @isTest
    private static void saveItemTest(){
        Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE stageName = 'Pitching' LIMIT 1];
        Test.startTest();
        String json = '[{"Amount__c":"10000","End_Date__c":"2021-02-05","Family__c":"Digital","Id":"'+pp.Id+'","Start_Date__c":"2021-02-01","Status__c":"Pipeline"}]';
        ProductSearchController.saveItems(json, opp.Id);
        Test.stopTest();
    }
    
    @isTest
    private static void saveItem_event_PositiveTest(){
        Pipeline_Product__c pp = [SELECT Id FROM Pipeline_Product__c LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE stageName = 'Pitching' LIMIT 1];
        Test.startTest();
        String json = '[{"Amount__c":"10000","End_Date__c":"2021-02-01","Family__c":"Digital","Id":"'+pp.Id+'","Start_Date__c":"2021-02-01","Status__c":"Pipeline"}]';
        ProductSearchController.saveItems(json, opp.Id);
        Test.stopTest();
    }
}