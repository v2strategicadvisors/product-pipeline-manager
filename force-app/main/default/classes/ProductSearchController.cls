public class ProductSearchController {

    /**
     * @description Gettting the Pipeline Products.
     * @param searchCriteria This is the searchCritieria recieved in the method as a JSON i.e. 
     * {
     *      searchCriteria : [ { Name :{type : 'text', value: 'test'} },
     *                          {Amount__c : {type: 'currency', value: '10.0'}}
     *                       ]
     * }
     * @param objectApiName This is the API name of the object to use.
     * @param fieldSetApiName This is the API name of the field set used for getting the specific fields for the object
     * @param limitRecords The amount of records returned in the query.
     * @return List<Pipeline_Products__c> This method will return a list of Pipleline_Products__c.
     */
    @AuraEnabled
    public static List<Pipeline_Product__c> getPipeLineProducts(String searchCriteria,String objectApiName, String fieldSetApiName,Integer limitRecords){
        List<Pipeline_Product__c> productPipeLineList = new List<Pipeline_Product__c>();
        try {
                String queryString = ProductSelectorHelper.getQueryString(searchCriteria, objectApiName, fieldSetApiName,limitRecords);
                List<Pipeline_Product__c> searchedProducts = Database.query(queryString);

                Map<Id,Pipeline_Product__c> productPipelineMap = new Map<Id,Pipeline_Product__c>(productPipelineList);

                List<User_Brand__c> userBrands = [SELECT Id,Pipeline_Brand__r.Brand_Name__c FROM User_Brand__c WHERE User__c =: UserInfo.getUserId()];
                System.debug('the user brands >> ' + userBrands);
                //To search for specific products if user only can see Brands assigned through user brands object.
                if(!userBrands.isEmpty()){
                    Map<Id,User_Brand__c> userBrandsMap = new Map<Id,User_Brand__c>(userBrands);
                    for(Id userBrandId :userBrandsMap.keySet() ){
                        User_Brand__c userBrandRecord = userBrandsMap.get(userBrandId);
                        for(Pipeline_Product__c pipelineProductRecord: searchedProducts){
                            if(pipelineProductRecord.Pipeline_Brand__r.Brand_Name__c.containsIgnoreCase(userBrandRecord.Pipeline_Brand__r.Brand_Name__c)){
                                productPipeLineList.add(pipelineProductRecord);
                            }
                        }
                    }
                } else {
                    productPipeLineList.addAll(searchedProducts);
                }

        } catch (Exception e) {
            throw new ProductSelectorHelper.ProductSelectorException(e.getMessage());
        }
        return productPipeLineList;
    }

    /**
     * @description This purpose of this method is to return the fields in
     *  the fieldset to be able to use these as the columns headers
     * @param fieldSetApiName This is the API name of the field set in the object specified
     * @param objectApiName This is the API of the object
     * @return List<ProductSelectorHelper.FieldSetWrapper> This is the list that will contain all the columns.
     */
    @AuraEnabled
    public static List<ProductSelectorHelper.FieldSetWrapper> getPipelineProductTableColumns (String fieldSetApiName, String objectApiName){
        List<ProductSelectorHelper.FieldSetWrapper> fieldSetWrapperList = new List<ProductSelectorHelper.FieldSetWrapper>();
        String namespacePrefix = ProductSelectorHelper.NAMESPACEPREFIX;
        String fieldSetName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + fieldSetApiName): fieldSetApiName;
        String objectName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + objectApiName): objectApiName;
        fieldSetWrapperList = ProductSelectorHelper.getObjectFieldSetFields(fieldSetName,objectName);
        return fieldSetWrapperList;
    }

    @AuraEnabled
    public static List<ProductSelectorHelper.FieldSetWrapper> getPipelineLineItemProductTableColumns (String fieldSetApiName, String objectApiName){
       return getPipelineProductTableColumns(fieldSetApiName, objectApiName);
    }

    /**
     * @description This method is for verifying if the configuration is set to get picklist values from the object
     * or based on the record type.
     * @param objectApiName The API name of the object for the picklist value
     * @param dveloperName The API name of the Custom Metadata Type record.
     * @param picklistFieldApiName The API of the picklist field to check related to the object.
     * @return Map<String,String> This will return one of two things that are :
     *  1) A map containing the Id and RecordType Label of the Record Type is it is set in the custom metadata type
     *  2) A Map containing the Value and Label of all picklist options
     */
    @AuraEnabled
    public static Map<String,String> checkForPickListValues(String objectApiName, String customMedatadeveloperName, String picklistFieldApiName, Boolean useRecordType, String recordTypeDeveloperName){
        Map<String,String> picklistValuesList = new Map<String,String>();
        try {
            //Product_Selector__mdt customMetadataTypeRecord = ProductSelectorHelper.getMetadataRecord(customMedatadeveloperName);
            Map<String,RecordTypeInfo> recordTypeMap = ProductSelectorHelper.getRecordTypes(objectApiName);
            //The configuration passed
            if(useRecordType){
                for(String recordTypeApiName: recordTypeMap.keySet()){
                    RecordTypeInfo recordTypeInfoRec = recordTypeMap.get(recordTypeApiName);
                    System.debug('THE RECORD TYPE INFO' + recordTypeInfoRec);
                    System.debug('THE RECORD TYPE INFO' + recordTypeDeveloperName);
                    if(recordTypeDeveloperName == recordTypeInfoRec.getDeveloperName()){
                        picklistValuesList.put(recordTypeInfoRec.getRecordTypeId(),recordTypeInfoRec.getName());
                        break;
                    }
                }
                
            } else {
                picklistValuesList.putAll(ProductSelectorHelper.getPicklistValuesFromObject(objectApiName, picklistFieldApiName));
            }
        }catch(Exception e){
            throw new ProductSelectorHelper.ProductSelectorException(e.getMessage());
        }
        
        return picklistValuesList;
    }

    /**
     * @description This method is for verifying if the configuration is set to get picklist values from the object
     * or based on the record type.
     * @param objectApiName The API name of the object for the picklist value
     * @param dveloperName The API name of the Custom Metadata Type record.
     * @param picklistFieldApiName The API of the picklist field to check related to the object.
     * @return Map<String,String> This will return one of two things that are :
     *  1) A map containing the Id and RecordType Label of the Record Type is it is set in the custom metadata type
     *  2) A Map containing the Value and Label of all picklist options
     */
    @AuraEnabled
    public static List<String> getBrandOptions(String userId, Boolean userUserBrandsObject){
        
        List<String> splittedBrandList = new List<String>();
        try {
            Product_Selector__mdt productSelectorConfiguration = ProductSelectorHelper.getMetadataRecord('Selector_Configuration');
            //Id userId = UserInfo.getUserId();
            List<String> brandList = new List<String>();
            if(userUserBrandsObject){
                List<User_Brand__c> userBrands = [SELECT Id,Pipeline_Brand__r.Brand_Name__c FROM User_Brand__c WHERE User__c =: userId];
                if(!userBrands.isEmpty()){
                    for(User_Brand__c userBrandRecord: userBrands){
                        splittedBrandList.add(userBrandRecord.Pipeline_Brand__r.Brand_Name__c);
                    }
                } else {
                    List<Pipeline_Brand__c> brandOptions = [SELECT Id,Brand_Name__c FROM Pipeline_Brand__c Order By Brand_Name__c];
                    for(Pipeline_Brand__c brand: brandOptions){
                        splittedBrandList.add(brand.Brand_Name__c);
                    }
                }
               
            } else {
                splittedBrandList = productSelectorConfiguration.Brand_Options__c.trim().split('\n');
                
                //This will remove the second line break the values have defaulted when entered
                //You can find more information on the Text Area Long Custom Type field documentation.
                for(String brand: splittedBrandList){
                    brandList.add(brand.deleteWhitespace().trim());
                }
                splittedBrandList = brandList;
            }
        }catch(Exception e){
            throw new ProductSelectorHelper.ProductSelectorException(e.getMessage());
        }
        return splittedBrandList;
    }

    /**
     * @description This method loads the configuration to the search UI in order to setup the correct data.
     * @return Product_Selector__mdt Custom Metadata Type needed for the configuration of the UI.
     */
    @AuraEnabled
    public static Product_Selector__mdt loadProductSelectorConfiguration(){
        Product_Selector__mdt productSelectorConfiguration = new Product_Selector__mdt();
        try {
            productSelectorConfiguration = ProductSelectorHelper.getMetadataRecord('Selector_Configuration');
        } catch (Exception e) {
            throw new ProductSelectorHelper.ProductSelectorException(e.getMessage());
        }
        return productSelectorConfiguration;
    }

    /**
     * @description This method is to get the namespace of the org.
     * @return String with the namespace.
     */
    @AuraEnabled
    public static String getNamespace(){
        return ProductSelectorHelper.NAMESPACEPREFIX;
    }

    /**Receiving the list: 
     * [
     *      {    Amount__c:10000, End_Date__c:2021-02-05, Family__c:Digital, 
     *           Id:a024x000003yHA8AAM, Start_Date__c:2021-02-01, Status__c:Pipeline
     *      }
     * ]
    */
    @AuraEnabled
    public static void saveItems(String jsonproductItems,Id opportunityId){
        ProductSelectorHelper.saveLineItems(jsonproductItems,opportunityId);
    }

    
   
}