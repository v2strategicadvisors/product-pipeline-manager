public inherited sharing class ProductSelectorManagerController {
    /**
     * @description Gettting the Pipeline_Product_Line_Item__c.
     * @param searchCriteria This is the searchCritieria recieved in the method as a JSON i.e. 
     * {
     *      searchCriteria : [ { Name :{type : 'text', value: 'test'} },
     *                          {Amount__c : {type: 'currency', value: '10.0'}}
     *                       ]
     * }
     * @param objectApiName This is the API name of the object to use.
     * @param fieldSetApiName This is the API name of the field set used for getting the specific fields for the object
     * @param limitRecords The amount of records returned in the query.
     * @return List<Pipeline_Product_Line_Item__c> This method will return a list of Pipeline_Product_Line_Item__c.
     */
    @AuraEnabled
    public static List<Pipeline_Product_Line_Item__c> getPipeLineProductLineItems(String searchCriteria,String objectApiName, String fieldSetApiName,Integer limitRecords){
        List<Pipeline_Product_Line_Item__c> pipeLineProductLineItemList = new List<Pipeline_Product_Line_Item__c>();
        try {
                String queryString = ProductSelectorHelper.getQueryString(searchCriteria, objectApiName, fieldSetApiName, limitRecords);
                pipeLineProductLineItemList = Database.query(queryString);

        } catch (Exception e) {
            throw new ProductSelectorHelper.ProductSelectorException(e.getMessage());
        }
        return pipeLineProductLineItemList;
    }

    /**
     * @description This purpose of this method is to return the fields in
     *  the fieldset to be able to use these as the columns headers
     * @param fieldSetApiName This is the API name of the field set in the object specified
     * @param objectApiName This is the API of the object
     * @return List<ProductSelectorHelper.FieldSetWrapper> This is the list that will contain all the columns.
     */
    @AuraEnabled
    public static List<ProductSelectorHelper.FieldSetWrapper> getPipelineLineItemTableColumns (String fieldSetApiName, String objectApiName){
        String namespacePrefix = ProductSelectorHelper.NAMESPACEPREFIX;
        String fieldSetName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + fieldSetApiName): fieldSetApiName;
        String objectName = (String.isNotBlank(namespacePrefix))? (namespacePrefix + objectApiName): objectApiName;
        List<ProductSelectorHelper.FieldSetWrapper> fieldSetWrapperList = ProductSelectorHelper.getObjectFieldSetFields(fieldSetName, objectName);
        return fieldSetWrapperList;
    }

    /**
     * @description Change the month of a Pipeline_Product_Line_Item__c start date.
     * @param processPipelineProductLineItemId This is the id of a Pipeline_Product_Line_Item__c.
     * @param month  This is the abbreviation of a month.
     * Available months are {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEPT, OCT, NOV DEC }
     * Example of use MonthSelectorController.processPipelineProductLIDate('a094x000000PrAkAAK', 'JAN');
     * @return None.
     */
    @AuraEnabled
    public static void processPipelineProductLIDate(Id processPipelineProductLineItemId, Integer month,Integer year){
        Pipeline_Product_Line_Item__c processPipelineProductLineItem = null;
        try {
            if(processPipelineProductLineItemId != null){
                processPipelineProductLineItem = [SELECT Id, Start_Date__c FROM Pipeline_Product_Line_Item__c WHERE Id =: processPipelineProductLineItemId ];
                modifyMonth(month,processPipelineProductLineItem,year); 
            }
        } catch (Exception e){
            throw new ProductSelectorHelper.ProductSelectorException('The Pipeline_Product_Line_Item__c wit id ' + processPipelineProductLineItemId + ' not foud');
        }
        
    }

    /**
     * @description modify the Start_Date__c month of Pipeline_Product_Line_Item__c object.
     * @param month This is the number of the month from (1 to 12).
     * @param ppli  This is the Pipeline_Product_Line_Item__c object.
     * @return None.
     */
    private static void modifyMonth(Integer month, Pipeline_Product_Line_Item__c ppli,Integer year){
        //checks if the variable month is not out of range
        if(month < 0 && month > 12){
            throw new ProductSelectorHelper.ProductSelectorException('Error, the number of the month is out of range (1 - 12)');
        }

        if(ppli != null){
            Integer monthAux = ppli.Start_Date__c.month();
            Integer day = ppli.Start_Date__c.day();
            Integer lineYear = ppli.Start_Date__c.year();

            if(monthAux != month || lineYear != year){
                //generates the new date with the new month and assign the value to the start date
                ppli.Start_Date__c = Date.parse(month+'/'+day+'/'+year);
                update ppli;
            }
        }
        
    }

    @AuraEnabled(cacheable=true)
    public static Map<String,String> getStatusValues(){
        Map<String,String> valuesMap = ProductSelectorHelper.getPicklistValuesFromObject('Pipeline_Product_Line_Item__c','Status__c');
        return valuesMap;
    }

    @AuraEnabled
    public static void saveStatus(String status, String pipelineLineItemId){
        try{
            Pipeline_Product_Line_Item__c pipelineLineItem = [SELECT Id,Status__c FROM Pipeline_Product_Line_Item__c WHERE Id =: pipelineLineItemId];
            pipelineLineItem.Status__c = status;
            update pipelineLineItem;
        }catch(Exception e){
            throw new ProductSelectorHelper.ProductSelectorException('The status was not saved. ' + e.getMessage());    
        }
    }

    @AuraEnabled
    public static String getNamespace(){
        return ProductSelectorHelper.NAMESPACEPREFIX;
    }
}